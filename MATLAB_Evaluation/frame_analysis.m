% 2D FFT Demo to blur an image, and show spectra of both original and blurred images.
clc;    % Clear the command window.
close all;  % Close all figures (except those of imtool.)
imtool close all;  % Close all imtool figures.
clear;  % Erase all existing variables.
workspace;  % Make sure the workspace panel is showing.
% format longg;
% format compact;
fontSize = 20;

% Change the current folder to the folder of this m-file.
if(~isdeployed)
	cd(fileparts(which(mfilename)));
end

refFileName  = 'scref';
[refImage,alpha] = exrread(refFileName + ".exr");
refImage = rgb2gray(refImage);
refMat = double(refImage);

rgb = tonemap(refImage);
imwrite(rgb,"RefToneMap.png","png");

baseFileName = "BaseImages/savedBaseImage";
mutFileName = "MutImages/savedDecImage";
blueFileName = "BlueImages/savedBlueImage";

imageCount = 10;

% Choose base vs mut computation (instead of for loop over both)
% fileName = baseFileName;
fileName = mutFileName;
% fileName = blueFileName;

totalCov = 0;
totalError = 0;

totImage = zeros([700,700]);

for i = 1:imageCount
    [colImage,~] = exrread(fileName + i + ".exr");
    % rgb = tonemap(colImage);
    % imwrite(rgb,baseFileName+"ToneMap.png","png");
    % imshow(rgb)
            
    [~, ~, numberOfColorChannels] = size(colImage);
    if numberOfColorChannels > 1
        grayImage = rgb2gray(colImage);
    end

    totImage = totImage + grayImage;
end

avgImage = totImage / imageCount;
avgMat = double(avgImage);

subplot(2, 2, 2);
imshow(avgImage)

for i = 1:imageCount
    [colImage,~] = exrread(fileName + i + ".exr");
    % rgb = tonemap(colImage);
    % imwrite(rgb,baseFileName+"ToneMap.png","png");
    % imshow(rgb)
            
    [~, ~, numberOfColorChannels] = size(colImage);
    if numberOfColorChannels > 1
        grayImage = rgb2gray(colImage);
    end
    grayMat = double(grayImage);
    
    % Negative sample covariance should also be possible if a pixel's
    % neighbours have a different sign.
    diffImage = (grayMat - avgMat);
            
    err = rmse(refMat, grayMat, "all");
    totalError = totalError + err;
    
    % subplot(2, 2, 3);
    % imshow(abs(diffImage))
    % subplot(2, 2, 4);
    % imshow(abs(diffImage2))
    
    boxSize = 8;
    nrBoxCells = boxSize*boxSize;
    halfBoxSize = ceil(boxSize/2);
    fun = @(x) mean(x(halfBoxSize,halfBoxSize) .* x,"all");

    % todo: be sure not to include i==j?
    % funwocenter = @(x) (double(fun(x) * nrBoxCells) - x(halfBoxSize,halfBoxSize)*x(halfBoxSize,halfBoxSize)) / double(nrBoxCells - 1.0);
    
    B = nlfilter(diffImage,[boxSize boxSize],fun);
    % B2 = nlfilter(diffImage,[boxSize boxSize],funwocenter);
    
    % maxDiff = max(diffImage(:))
    % maxB = max(B(:))
    % maxBImage = max(BImage(:))
    % meanB = mean(B,"all");
    meanB = mean(B(:))
    totalCov = totalCov + meanB;

end

meanCov = totalCov / imageCount
meanErr = totalError / imageCount

diffImageIm = mat2gray(diffImage);
subplot(2, 2, 4);
imshow(diffImageIm)

BImage = mat2gray(B);
subplot(2, 2, 3);
imshow(BImage)
subplot(2, 2, 1);
imshow(grayImage)


% 
% imwrite(BImage,baseFileName+"COV.png","png");
% 
% imwrite(BImage,mutFileName+"COV.png","png");

% 
% 
% % fileName = "BlueImages/savedBlueImage1";
% [grayImage,alpha] = exrread(fileName + "1.exr");
% grayImage = rgb2gray(grayImage);
% 
% figure();
% % title('Original Gray Scale Image', 'FontSize', fontSize)
% % % 
% targetSize = [128 128];
% r = centerCropWindow2d(size(grayImage),targetSize);
% grayImage = imcrop(grayImage,r);
% 
% subplot(1, 2, 1);
% imshow(grayImage);
% imwrite(grayImage,fileName+"Cropped.png","png");
% % 
% 
% % % Test the DFT of the sample mask
% % grayImage = imread("bluenoisegray.png");
% % grayImage = rgb2gray(grayImage);
% 
% 
% % Perform 2D FFTs
% fftOriginal = fft2(double(grayImage));
% 
% logShiftedFFT = log(abs(fftshift(fftOriginal)));
% 
% % Display magnitude and phase of 2D FFTs
% subplot(1, 2, 2);
% imshow(mat2gray(logShiftedFFT),[]);
% imcontrast
% title('Log Magnitude of Spectrum', 'FontSize', fontSize)
% 
% dftImage = mat2gray(logShiftedFFT);
% 
% imwrite(dftImage,fileName+"DFT.png","png");
