Code for the thesis project 'Blue Noise Distributed MCMC Decorrelation of ReSTIR'.

Using the code for ReSTIR PT as basis (https://github.com/DQLin/ReSTIR_PT), we (re)implement a paper by Sawhney et al. on adding MCMC mutations to decorrelate ReSTIR PT [[1]](#1).
We also modify the acceptance probability to optimise for blue noise, though with marginal results.

See below for a short video demonstrating the difference between the implemented decorrelated ReSTIR and base ReSTIR PT.

![](Demo.mp4)

Run 'RunReSTIRPT.bat' to start the program.
The used scene is found at '\Media\VeachAjar\'.
Rendergraphs are found at '\Source\Mogwai\Data'.

## References
<a id="1">[1]</a> 
Sawhney, R., Lin, D., Kettunen, M., Bitterli, B., Ramamoorthi, R., Wyman, C., and Pharr, M. (2022). Decorrelating restir samplers via mcmc mutations. https://doi.org/10.48550/arXiv.2211.00166.
