#pragma once
#include "Falcor.h"

namespace Falcor
{

    /** Enumeration of available debug outputs.
        Note: Keep in sync with definition in Params.slang
    */
    enum class DebugOutput
    {
        Disabled,
        Position,
        Depth,
        Normal,
        FaceNormal,
        DiffuseWeight,
        SpecularWeight,
        SpecularRoughness,
        PackedNormal,
        PackedDepth,
        InitialWeight,
        TemporalReuse,
        Decorrelation,
        SpatialReuse,
        FinalSampleDir,
        FinalSampleDistance,
        FinalSampleLi,
    };


    enum class ReSTIRMode
    {
        InputOnly = 0,
        TemporalOnly = 1,
        TemporalAndBiasedSpatial = 2,
        TemporalAndUnbiasedSpatial = 3
    };

    enum class TargetPDF
    {
        IncomingRadiance = 0,
        OutgoingRadiance = 1
    };

    /** Configuration options.
    */
    struct Options
    {
        using SharedPtr = std::shared_ptr<Options>;
        static SharedPtr create() { return SharedPtr(new Options()); };
        static SharedPtr create(const Options& other) { return SharedPtr(new Options(other)); };

        Options() { }
        Options(const Options& other) { *this = other; }
        // Common Options for ReSTIR DI and GI.

        bool useReSTIRDI = true;
        bool useReSTIRGI = false;
        float normalThreshold = 0.5f;               ///< Normal cosine threshold for reusing temporal samples or spatial neighbor samples.
        float depthThreshold = 0.1f;                ///< Relative depth threshold for reusing temporal samples or spatial neighbor samples.

        // Options for ReSTIR DI only.

        // Light sampling options.
        float envLightWeight = 1.f;                 ///< Relative weight for selecting the env map when sampling a light.
        float emissiveLightWeight = 1.f;            ///< Relative weight for selecting an emissive light when sampling a light.
        float analyticLightWeight = 1.f;            ///< Relative weight for selecting an analytical light when sampling a light.

        bool useEmissiveTextureForSampling = true; ///< Use emissive texture for light sample evaluation.
        bool useEmissiveTextureForShading = true;  ///< Use emissive texture for shading.
        bool useLocalEmissiveTriangles = false;      ///< Use local emissive triangle data structure (for more efficient sampling/evaluation).

        // Light tile options.
        uint32_t lightTileCount = 128;              ///< Number of light tiles to compute.
        uint32_t lightTileSize = 1024;              ///< Number of lights per light tile.

        // Visibility options.
        bool useAlphaTest = true;                   ///< Use alpha testing on non-opaque triangles.
        bool useInitialVisibility = true;           ///< Check visibility on inital sample.
        bool useFinalVisibility = true;             ///< Check visibility on final sample.
        bool reuseFinalVisibility = false;          ///< Reuse final visibility temporally.

        // Initial resampling options.
        uint32_t screenTileSize = 8;                ///< Size of screen tile that samples from the same light tile.
        uint32_t initialLightSampleCount = 32;      ///< Number of initial light samples to resample per pixel.
        uint32_t initialBRDFSampleCount = 1;        ///< Number of initial BRDF samples to resample per pixel.
        float brdfCutoff = 0.f;                     ///< Value in range [0,1] to determine how much to shorten BRDF rays.

        // Temporal resampling options.
        bool useTemporalResampling = true;          ///< Enable temporal resampling.
        uint32_t maxHistoryLength = 20;             ///< Maximum temporal history length.


        // Spatial resampling options.
        bool useSpatialResampling = true;           ///< Enable spatial resampling.
        uint32_t spatialIterations = 1;             ///< Number of spatial resampling iterations.
        uint32_t spatialNeighborCount = 5;          ///< Number of neighbor samples to resample per pixel and iteration.
        uint32_t spatialGatherRadius = 30;          ///< Radius to gather samples from.

        // General options.
        bool usePairwiseMIS = true;                 ///< Use pairwise MIS when combining samples.
        bool unbiased = true;                      ///< Use unbiased version of ReSTIR by querying extra visibility rays.

        DebugOutput debugOutput = DebugOutput::Disabled;

        bool enabled = true;  // (I think) now controls both ReSTIR GI and DI

        // Options for ReSTIR GI only.

        ReSTIRMode reSTIRMode = ReSTIRMode::TemporalAndUnbiasedSpatial; ///< ReSTIR GI Mode.
        TargetPDF targetPdf = TargetPDF::OutgoingRadiance;  ///< Target function mode.
        uint32_t reSTIRGITemporalMaxSamples = 30;           ///< Maximum M value for temporal reuse stage.
        uint32_t reSTIRGISpatialMaxSamples = 100;           ///< Maximum M value for spatial reuse stage.
        uint32_t reSTIRGIReservoirCount = 1;                ///< Number of reservoirs per pixel.
        bool reSTIRGIUseReSTIRN = true;
        uint32_t reSTIRGIMaxSampleAge = 100;                ///< Maximum frames that a sample can survive.
        float diffuseThreshold = 0.f;// 0.17f;                     ///< Pixels with diffuse component under this threshold will not use ReSTIR GI.
        float reSTIRGISpatialWeightClampThreshold = 10.f;
        bool reSTIRGIEnableSpatialWeightClamping = true;
        float reSTIRGIJacobianClampTreshold = 10.f;
        bool reSTIRGIEnableJacobianClamping = false;
        bool reSTIREnableTemporalJacobian = true;

        bool forceClearReservoirs = false;                  ///< Force clear temporal and spatial reservoirs.
    };
}
